## Caso de estudio 2

Este caso de estudio consiste en desarrollar una aplicación móvil para automatizar el sistema de ventas de una cadena de tienadas por departamentos, de una empresa comercial que opera grandes establecimientos tipo bodega.
En este caso, el nombre de la empresa (ficticia) es "UDGMart".

### Desarrollado para Seminario de Ingeniería de Software
### Profesor: Luis Antonio Medellín Serna
### Alumno: Josué Renaud Hernández Guzmán - 217294598