import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { InAppBrowser } from '@ionic-native/in-app-browser';

import { ProductsService } from '../providers/products.service';
import { ClientsService } from '../providers/clients.service';
import { OrdersService } from '../providers/orders.service';
import { MapService } from '../providers/map.service';

import { HttpClientModule } from '@angular/common/http';
import { AuthProvider } from '../providers/auth.service';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';

import { IonicStorageModule } from '@ionic/storage';

import { Geolocation } from '@ionic-native/geolocation';

const configFirebase = {
  apiKey: "AIzaSyBt5VnnLB-eS-ZRLvGfQZVeuJqSCS5Sdg8",
  authDomain: "puntodeventa-d6c1f.firebaseapp.com",
  databaseURL: "https://puntodeventa-d6c1f.firebaseio.com",
  projectId: "puntodeventa-d6c1f",
  storageBucket: "puntodeventa-d6c1f.appspot.com",
  messagingSenderId: "803325499348"
};

@NgModule({
  declarations: [
  MyApp
  ],
  imports: [
  BrowserModule,
  HttpClientModule,
  IonicStorageModule.forRoot({
    name: '__mydb',
    driverOrder: ['indexeddb', 'sqlite', 'websql']
  }),
  IonicModule.forRoot(MyApp),
  AngularFireModule.initializeApp( configFirebase ),
  AngularFireDatabaseModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
  MyApp
  ],
  providers: [
  StatusBar,
  SplashScreen,
  InAppBrowser,
  Geolocation,
  {provide: ErrorHandler, useClass: IonicErrorHandler},
  ProductsService,
  ClientsService,
  AuthProvider,
  OrdersService,
  MapService
  ]
})
export class AppModule {}
